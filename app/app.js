import angular from 'angular';

export default angular.module('app', [
    'ngResource',
    'ngSanitize',
    'ui.router',

    'geolocationApiModule',
    'geocodeServiceModule',

    'mapModule',
    'infoPopupModule',
    'mapPageModule',

//    'ngMockE2E'
]);