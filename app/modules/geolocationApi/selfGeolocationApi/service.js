export default class Service {

    constructor ($resource, $q) {
        this._$q = $q;
        this._resource = $resource('https://cloudnet.kz/api/v1');
    }

    getLocation () {
        var params = {
            jsonrpc: '2.0',
            id: Math.random() * Math.pow(10, 17) + 1,
            method: 'getData',
            params: [
                '5784fa07-8b19-405d-bbbe-a932ab5819b3'
            ],
        };

        var defer = this._$q.defer();

        this._resource.save(params).$promise.then(
            (response) => (!!response.error) ? defer.reject(response.error) : defer.resolve(response.result),
            (response) => defer.reject(response)
        );

        defer.$promise = defer.promise;

        return defer;
    }
}

Service.$inject = ['$resource', '$q'];