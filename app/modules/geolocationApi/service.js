export default class Service {

    constructor (selfGeolocationApiService) {
        this.service = selfGeolocationApiService
    }

    getLocation () {
        return this.service.getLocation();
    }
}

Service.$inject = ['selfGeolocationApiService'];