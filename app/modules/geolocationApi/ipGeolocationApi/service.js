export default class Service {

    constructor ($resource) {
        this._resource = $resource('http://ip-api.com/json');
    }

    getLocation () {
        return this._resource.get();
    }
}

Service.$inject = ['$resource'];
