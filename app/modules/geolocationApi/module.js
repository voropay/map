import angular from 'angular';
import IpGeolocationApiService from './ipGeolocationApi/service';
import SelfGeolocationApiService from './selfGeolocationApi/service';
import GeolocationApiService from './service';

export default angular.module('geolocationApiModule', [])
    .service('ipGeolocationApiService', IpGeolocationApiService)
    .service('selfGeolocationApiService', SelfGeolocationApiService)
    .service('geolocationApiService', GeolocationApiService);
