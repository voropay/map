import module from './module';


class Service {
    constructor ($q) {
        this.$q = $q;
    }

    reverseGeocode (lat, lon) {
        let deferred = this.$q.defer();

        let geocoder = new google.maps.Geocoder;
        let params = {
            location: new google.maps.LatLng(lat, lon),
        };

        geocoder.geocode(params, (result, status) => {
            console.dir(result);

            if (status !== 'OK' || !result[1]) {
                deferred.reject(status);
                return;
            }

            deferred.resolve(result[1].formatted_address);
        });

        return deferred.promise;
    }
}

Service.$inject = ['$q'];

module.service('geocodeService', Service);
