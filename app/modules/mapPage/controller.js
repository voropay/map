import module from './module';



class Controller {
    constructor (geolocationApiService) {
        this.geolocationApiService = geolocationApiService;

        this.init();
    }

    init () {
        this.coordsPromise = this.loadCoords();
    }

    isLoaded () {
        return (!!this.coordsPromise) && ([1, 2].indexOf(this.coordsPromise.$$state.status) !== -1);
    }

    loadCoords () {
        return this.geolocationApiService.getLocation().$promise.then(response => {
            this.coords = {
                lat: response.lat,
                lon: response.lon
            };

            return response;
        });
    }

}


Controller.$inject = ['geolocationApiService'];

module.controller('mapPageController', Controller);