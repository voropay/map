import app from './../../app';
import stub from './stub';


app.run(['$httpBackend', function ($httpBackend) {
    $httpBackend.whenGET('api/page-header/menu').respond(() => [200, stub.menu]);
}]);
