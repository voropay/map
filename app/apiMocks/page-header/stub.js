class Stub {

    constructor() {
        this.menu = [
            {
                index: 1,
                title: 'Jobs',
                pageId: 1,
            },
            {
                index: 2,
                title: 'Kontakt',
                pageId: 2,
            },
            {
                index: 3,
                title: 'Geschäftsführung',
                pageId: 3,
            },
            {
                index: 4,
                title: 'Impressum',
                pageId: 4
            },
        ];
    }

}

export default new Stub();
