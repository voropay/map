import app from './../app';

//import './employeeApi/mock';



app.run(['$httpBackend', function($httpBackend) {

    //CMS content
    $httpBackend.whenGET(/\.(html|css)$/).passThrough();
}]);
