'use strict';

import 'jquery';
import 'bootstrap';
import 'angular';
import 'angular-resource';
import 'angular-sanitize';
import 'angular-ui-router';
//import 'angular-mocks';

import './app';
import './routes';
import './resourceURL';
import './modules/geolocationApi/module';
import './modules/geocodeService/define';
import './modules/mapPage/define';

import './components/map/define'
import './components/infoPopup/define'

//import './apiMocks/define';


angular.bootstrap(document, ['app']);