import angular from 'angular';

class Controller {
    constructor ($scope, $templateRequest, $compile, geocodeService) {
        this.$scope = $scope;
        this.$templateRequest = $templateRequest;
        this.$compile = $compile;
        this.geocodeService = geocodeService;

        this.init();
    }

    init () {
        this.templateUrl = 'components/infoPopup/index.html';

        let lat = this.lat || 0.0000,
            lon = this.lon || 0.0000;

        this.geocodeService.reverseGeocode(lat, lon)
            .then((data) => this.text = data)
            .finally(() => this.createInfo(lat, lon, this.map));
    }

    createInfo (lat, lon, map) {
        let coords = new google.maps.LatLng(lat, lon);

        this.getContent().then((domElement) => {
            let listener = map.addListener('tilesloaded', () => {
                let info = new google.maps.InfoWindow({
                    content: domElement,
                    position: coords,
                });

                info.open(map);

                google.maps.event.removeListener(listener);
            });

        });
    }

    getContent () {
        return this.$templateRequest(this.templateUrl).then((html) => {
            var template = angular.element(html);

            this.$compile(template)(this.$scope);

            return template[0];
        });

    }

}


Controller.$inject = ['$scope', '$templateRequest', '$compile', 'geocodeService'];

export default Controller;