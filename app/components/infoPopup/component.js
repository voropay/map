import module from './module';
import controller from './controller';


let component = {
    controller: controller,
    controllerAs: 'ctrl',
    bindings: {
        map: '<',
        lat: '<',
        lon: '<',
    }
};


module.component('infoPopup', component);