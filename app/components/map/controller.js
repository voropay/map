class Controller {
    constructor ($element, $q) {
        this.$q = $q;

        this.init($element[0]);
    }

    init (domElement) {
        let lat = this.lat || 0.0000,
            lon = this.lon || 0.0000;

        // let marker = this.createMarker(coords, map);

        this.map = this.createMap(domElement, lat, lon);
    }

    createMap (domElement, lat, lon) {
        let coords = new google.maps.LatLng(lat, lon);

        let mapOptions = {
            zoom: this.zoom,
            center: coords,
            mapTypeId: google.maps.MapTypeId.TERRAIN
        };

        return new google.maps.Map(domElement, mapOptions);
    }

    createMarker (coords, map) {
        return new google.maps.Marker({
            position: coords,
            map: map,
        });
    }


}


Controller.$inject = ['$element', '$q'];

export default Controller;