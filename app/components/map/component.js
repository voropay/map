import module from './module';
import controller from './controller';


let component = {
    template: '<div class="map-component"></div>',
    controller: controller,
    controllerAs: 'ctrl',
    bindings: {
        lat: '<?',
        lon: '<?',
        zoom: '<?',
        map: '=?',
    }
};


module.component('map', component);