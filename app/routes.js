import app from './app';

app.config(routesConfig);

routesConfig.$inject = ['$stateProvider', '$urlRouterProvider'];

function routesConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('map', {
            url: '/map',
            templateUrl: './modules/mapPage/index.html',
            controller: 'mapPageController',
            controllerAs: 'ctrl'
        })


    $urlRouterProvider.otherwise('/map');
}