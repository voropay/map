var webpack = require("webpack");
var path = require("path");
var BowerWebpackPlugin = require("bower-webpack-plugin");

module.exports =  {
    context: path.resolve(__dirname, "app"),
    entry: "./main",
    output: {
        path: path.resolve(__dirname, "dist/webpack_dist"),
        filename: "boundle.js"
    },

    module: {
        loaders: [
            {
                test: /.js$/,

                include: [
                    path.resolve(__dirname, "app")
                ],

                exclude: [
                    path.resolve(__dirname, "app/bower_components")
                ],

                loader: "babel-loader",
            },
            {
                test: /[\/]angular\.js$/,
                loader: 'exports?angular'
            }
        ],
        noParse: [
            /angular\/angular\.js/,
            /jquery\.js/,
        ]
    },

    resolve: {
        modulesDirectories: ['app/bower_components'],
        extensions: ['', '.js']
    },

    plugins: [

        new BowerWebpackPlugin({
            modulesDirectories: ["app/bower_components"],
            manifestFiles:      "bower.json",
            includes:           /\.js$/,
            excludes:           [],
            searchResolveModulesDirectories: true
        }),
        new webpack.ProvidePlugin({
            "jQuery": "jquery",
            "$": "jquery",
            "window.jQuery": "jquery",
            "window.$": "jquery",
        })
    ],

    devtool: "source-map"
};