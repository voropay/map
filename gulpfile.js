var gulp = require('gulp'),
    clean = require('gulp-clean'),
    concat = require('gulp-concat'),
    bower = require('gulp-bower'),
    connect = require('gulp-connect'),
    gutil = require('gulp-util'),
    webpack = require('webpack'),
    q = require('q'),
    mergeStream = require('merge-stream');
//    babel = require('gulp-babel');
//    webpack = require('webpack-stream'),
//    browserify = require('browserify'),
//    transform = require('vinyl-transform'),
//    through2 = require('through2');

var babelConfig = {
    ignore: /app\/bower_components/,
};

var webpackConfig = require('./webpack.config');

var paths = {
    app: {
        js: ['./app/**/*.js'],
        html: ['app/**/*.html'],
        css: ['app/**/*.css'],
    },
    dist: {
        js: ['dist/**/*.js', 'dist/**/*.js.map'],
        html: ['dist/**/*.html'],
        css: ['dist/**/*.css'],
    }
};

paths.app.resources = [].concat(paths.app.html, paths.app.css);
paths.dist.resources = [].concat(paths.dist.html, paths.dist.css);

//gulp.task('default', ['server', 'watch']);
gulp.task('default', ['copy-images', 'webpack', 'copy-css', 'copy-html', 'server', 'watch']);

gulp.task('server', ['webpack', 'copy-css', 'copy-html'], function () {
    connect.server({
        port: 3003,
        root: 'dist',
        livereload: true
    });
});

/*
 gulp.task('watch', function () {
 gulp.watch(['./app/!*.html'], ['html']);
 gulp.watch(['./app/!*.js'], ['js']);
 // gulp.watch(['./app/!*.css'], ['css']);
 });

 gulp.task('html', function () {
 gulp.src('./app/!*.html')
 .pipe(connect.reload());
 });

 gulp.task('js', function () {
 gulp.src('./app/!*.js')
 .pipe(connect.reload());
 });
 */

/*
 gulp.task('css', function () {
 gulp.src('./app/!*.css')
 .pipe(connect.reload());
 });
 */

// load vendor dependencies 
gulp.task('bower', function () {
    return bower();
});

gulp.task('clean', function () {
    return gulp.src('dist', {read: false})
        .pipe(clean());
});

gulp.task('clean-js', function () {
    return gulp.src(paths.dist.js, {read: false})
        .pipe(clean());
});

gulp.task('clean-html', function () {
    return gulp.src(paths.dist.html, {read: false})
        .pipe(clean());
});

gulp.task('clean-css', function () {
    return gulp.src(paths.dist.css, {read: false})
        .pipe(clean());
});

gulp.task('copy-html', ['clean-html'], function () {
    return gulp.src(paths.app.html)
        .pipe(gulp.dest('dist'));
});

gulp.task('webpack', ['clean-js'], function (callback) {
    webpack(
        webpackConfig,
        function(err, stats) {
            if (err) {
                throw new gutil.PluginError("webpack", err);
            }

            gutil.log("[webpack]", stats.toString({}));

            callback();
        });
});

gulp.task('copy-css', ['clean-css'], function () {
    var appStyles = gulp.src(['./app/**/*.css', '!./app/bower_components/**/*.css'])
        .pipe(concat('boundle.css'))
        .pipe(gulp.dest('dist'));

    var vendorStyles = gulp.src('./app/bower_components/**/*.css')
        .pipe(gulp.dest('dist'));

    return mergeStream(appStyles, vendorStyles);
});

gulp.task('watch', ['server'], function () {
    gulp.watch(paths.app.js, ['watch-js']);
    gulp.watch(paths.app.css, ['watch-css']);
    gulp.watch(paths.app.html, ['watch-html']);
});

gulp.task('watch-js', ['webpack'], function () {
   connect.reload();
});

gulp.task('watch-css', ['copy-css'], function () {
    connect.reload();
});

gulp.task('watch-html', ['copy-html'], function () {
    connect.reload();
});


// delete this
gulp.task('copy-images', function () {
    return gulp.src('./images/**/*.*')
        .pipe(gulp.dest('dist/images'));
});
